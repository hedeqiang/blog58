<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>itool</title>
    <!-- Styles -->

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
            position: absolute;
        }

        .title img {
            /*font-size: 84px;*/
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .title a img {
            max-height: none !important;
            width: 120px;
            height: 120px;
            overflow: hidden;
            border-radius: 50%;
            -webkit-transition: -webkit-transform 600ms;
            -moz-transition: -moz-transform 600ms;
            transition: transform 600ms;
        }

        .title a img:hover {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            transform: rotate(360deg);
        }

        /* ---- reset ---- */

        body {
            margin: 0;
            font:normal 75% Arial, Helvetica, sans-serif;
        }

        canvas {
            display: block;
            vertical-align: bottom;
        }

        /* ---- particles.js container ---- */

        #particles-js {
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: #2ab27b;
            /*background-image: url("");*/
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 50% 50%;
        }

        /* ---- stats.js ---- */

        .count-particles{
            background: #000022;
            position: absolute;
            top: 48px;
            left: 0;
            width: 80px;
            color: #13E8E9;
            font-size: .8em;
            text-align: left;
            text-indent: 4px;
            line-height: 14px;
            padding-bottom: 2px;
            font-family: Helvetica, Arial, sans-serif;
            font-weight: bold;
        }

        .js-count-particles{
            font-size: 1.1em;
        }

        #stats,
        .count-particles{
            -webkit-user-select: none;
        }

        #stats{
            border-radius: 3px 3px 0 0;
            overflow: hidden;
        }

        .count-particles{
            border-radius: 0 0 3px 3px;
        }

    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div id="particles-js"></div>

    <div class="content">
        <div class="title m-b-md">
            <a href="/">
                <img id="avatar" class="avatar"
                     src="https://laravelcode.cn/uploads/images/posts/201901/24/1_1548340493_FRKyNBp2bL.png" alt="">
            </a>
        </div>

        <div class="links">
            <a target="_blank" href="https://laravelcode.cn">Blog</a>
            <a target="_blank" href="https://github.com/hedeqiang">GitHub</a>
            <a target="_blank" href="https://weibo.com/laravelcode">Weibo</a>
            <a target="_blank" href="https://twitter.com/hedeqiang666">Twitter</a>
        </div>
    </div>
</div>

<!-- scripts -->
<script src="{{ asset('js/particles.js') }}"></script>
<script>
    particlesJS.load('particles-js', '/js/particles.json', function() {
        console.log('callback - particles.js config loaded');
    });
</script>
</body>
</html>
